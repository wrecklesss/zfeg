SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `zfeg` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `zfeg` ;

-- -----------------------------------------------------
-- Table `zfeg`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zfeg`.`users` ;

CREATE TABLE IF NOT EXISTS `zfeg`.`users` (
  `usr_seq` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `usr_email` VARCHAR(50) NOT NULL,
  `usr_password` CHAR(32) NOT NULL,
  `usr_name` VARCHAR(30) NOT NULL,
  `usr_phone` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`usr_seq`),
  UNIQUE INDEX `UNIQUE` (`usr_email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfeg`.`categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zfeg`.`categories` ;

CREATE TABLE IF NOT EXISTS `zfeg`.`categories` (
  `cat_seq` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_name` VARCHAR(45) NOT NULL,
  `cat_depth` VARCHAR(45) NULL,
  `cat_count` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`cat_seq`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfeg`.`boards`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zfeg`.`boards` ;

CREATE TABLE IF NOT EXISTS `zfeg`.`boards` (
  `brd_seq` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `brd_name` VARCHAR(45) NOT NULL,
  `brd_desc` VARCHAR(255) NULL,
  `brd_skin` VARCHAR(45) NOT NULL DEFAULT 'default',
  PRIMARY KEY (`brd_seq`),
  UNIQUE INDEX `UNIQUE` (`brd_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfeg`.`documents`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zfeg`.`documents` ;

CREATE TABLE IF NOT EXISTS `zfeg`.`documents` (
  `doc_seq` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `doc_idx` INT UNSIGNED NOT NULL,
  `doc_brd_seq` INT UNSIGNED NOT NULL,
  `doc_cat_seq` SMALLINT UNSIGNED NULL,
  `doc_depth` VARCHAR(45) NULL,
  `doc_usr_seq` INT UNSIGNED NULL,
  `doc_usr_name` VARCHAR(50) NULL,
  `doc_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `doc_subject` VARCHAR(255) NOT NULL,
  `doc_link` VARCHAR(255) NULL,
  `doc_content` TEXT NULL,
  PRIMARY KEY (`doc_seq`),
  UNIQUE INDEX `UNIQUE` (`doc_idx` ASC, `doc_depth` ASC),
  INDEX `fk_documents_users` (`doc_usr_seq` ASC),
  INDEX `fk_documents_categories` (`doc_cat_seq` ASC),
  INDEX `fk_documents_boards` (`doc_brd_seq` ASC),
  CONSTRAINT `fk_documents_users`
    FOREIGN KEY (`doc_usr_seq`)
    REFERENCES `zfeg`.`users` (`usr_seq`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_documents_categories`
    FOREIGN KEY (`doc_cat_seq`)
    REFERENCES `zfeg`.`categories` (`cat_seq`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_documents_boards`
    FOREIGN KEY (`doc_brd_seq`)
    REFERENCES `zfeg`.`boards` (`brd_seq`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = '통합 문서 관리 테이블';


-- -----------------------------------------------------
-- Table `zfeg`.`comments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zfeg`.`comments` ;

CREATE TABLE IF NOT EXISTS `zfeg`.`comments` (
  `cmt_seq` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cmt_doc_seq` INT UNSIGNED NOT NULL,
  `cmt_message` VARCHAR(255) NOT NULL,
  `cmt_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cmt_seq`),
  INDEX `fk_comments_documents` (`cmt_doc_seq` ASC),
  CONSTRAINT `fk_comments_documents1`
    FOREIGN KEY (`cmt_doc_seq`)
    REFERENCES `zfeg`.`documents` (`doc_seq`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfeg`.`files`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zfeg`.`files` ;

CREATE TABLE IF NOT EXISTS `zfeg`.`files` (
  `fil_seq` INT UNSIGNED NOT NULL,
  `fil_doc_seq` INT UNSIGNED NOT NULL,
  `fil_path` VARCHAR(255) NOT NULL,
  `fil_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`fil_seq`),
  INDEX `fk_files_documents` (`fil_doc_seq` ASC),
  CONSTRAINT `fk_files_documents`
    FOREIGN KEY (`fil_doc_seq`)
    REFERENCES `zfeg`.`documents` (`doc_seq`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfeg`.`sessions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zfeg`.`sessions` ;

CREATE TABLE IF NOT EXISTS `zfeg`.`sessions` (
  `sid` CHAR(32) NOT NULL,
  `sid_maketime` INT UNSIGNED NULL,
  `sid_lifetime` INT UNSIGNED NULL,
  `sid_data` TEXT NOT NULL,
  PRIMARY KEY (`sid`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `zfeg`.`categories`
-- -----------------------------------------------------
START TRANSACTION;
USE `zfeg`;
INSERT INTO `zfeg`.`categories` (`cat_seq`, `cat_name`, `cat_depth`, `cat_count`) VALUES (1, 'test1', NULL, 1);
INSERT INTO `zfeg`.`categories` (`cat_seq`, `cat_name`, `cat_depth`, `cat_count`) VALUES (2, 'test2', NULL, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `zfeg`.`boards`
-- -----------------------------------------------------
START TRANSACTION;
USE `zfeg`;
INSERT INTO `zfeg`.`boards` (`brd_seq`, `brd_name`, `brd_desc`, `brd_skin`) VALUES (1, 'test', 'This is test Board', 'default');

COMMIT;


-- -----------------------------------------------------
-- Data for table `zfeg`.`documents`
-- -----------------------------------------------------
START TRANSACTION;
USE `zfeg`;
INSERT INTO `zfeg`.`documents` (`doc_seq`, `doc_idx`, `doc_brd_seq`, `doc_cat_seq`, `doc_depth`, `doc_usr_seq`, `doc_usr_name`, `doc_time`, `doc_subject`, `doc_link`, `doc_content`) VALUES (1, 4294967295, 1, 1, NULL, NULL, NULL, NULL, 'test', NULL, 'test');
INSERT INTO `zfeg`.`documents` (`doc_seq`, `doc_idx`, `doc_brd_seq`, `doc_cat_seq`, `doc_depth`, `doc_usr_seq`, `doc_usr_name`, `doc_time`, `doc_subject`, `doc_link`, `doc_content`) VALUES (2, 4294967295, 1, 2, '000', NULL, NULL, NULL, 'test', NULL, 'test');
INSERT INTO `zfeg`.`documents` (`doc_seq`, `doc_idx`, `doc_brd_seq`, `doc_cat_seq`, `doc_depth`, `doc_usr_seq`, `doc_usr_name`, `doc_time`, `doc_subject`, `doc_link`, `doc_content`) VALUES (3, 4294967294, 1, NULL, NULL, NULL, NULL, NULL, 'test', NULL, 'test');

COMMIT;


-- -----------------------------------------------------
-- Data for table `zfeg`.`comments`
-- -----------------------------------------------------
START TRANSACTION;
USE `zfeg`;
INSERT INTO `zfeg`.`comments` (`cmt_seq`, `cmt_doc_seq`, `cmt_message`, `cmt_time`) VALUES (1, 1, 'test', NULL);

COMMIT;

