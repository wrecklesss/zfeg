<?php

/**
 * Class Zfeg_Controller_Action_Board
 */
abstract class Zfeg_Controller_Action_Board extends Zfeg_Controller_Action implements Zfeg_Controller_Action_Board_interface
{
	protected $_brd_seq;
	protected $_brd_name;
	protected $_brd_desc;
	protected $_brd_skin;
	protected $_brd_file_path;
	protected $_page;
	protected $_row_count;

	/**
	 * @throws Exception
	 * @throws Zend_Db_Table_Exception
	 */
	public function init()
	{
		parent::init();

		$this->setPage($this->getRequest()->getParam('page'));
		if (!$this->getPage()) $this->setPage(1);

		$this->setRowCount($this->getRequest()->getParam('size'));
		if (!$this->getRowCount()) $this->setRowCount(10);

		$this->setBrdSeq($this->getRequest()->getParam('brd-seq'));
		if (!$this->getBrdSeq()) $this->setBrdSeq(1);

		$model = new Zfeg_Model_Boards();
		$entry = $model->find($this->getBrdSeq())->current();
		if ($entry) $this->setOptions($entry->toArray());
		else throw new Exception('Not Created Board');

		$this->view->board = $entry->toArray();

		$this->setBrdFilePath(APPLICATION_PATH . '/../data/upload/board/' . $this->getBrdSeq());
	}

	/**
	 * @param $name
	 * @param $value
	 * @return mixed|void
	 * @throws Exception
	 */
	public function __set($name, $value)
	{
		$method = 'set' . $name;
		if (!method_exists($this, $method))
			throw new Exception('Invalid property');
		$this->$method($value);
	}

	/**
	 * @param $name
	 * @return mixed
	 * @throws Exception
	 */
	public function __get($name)
	{
		$method = 'get' . $name;
		if (!method_exists($this, $method)) :
			throw new Exception('Invalid property');
		endif;
		return $this->$method();
	}

	/**
	 * @param Array $options
	 * @return $this
	 * @throws Exception
	 */
	public function getOptions(Array $options)
	{
		foreach ($options as $key => $value)
			$this->__get(str_replace(' ', '', ucwords(strtr($key, '_-', ' '))), $value);
		return $this;
	}

	/**
	 * @param Array $options
	 * @return $this
	 * @throws Exception
	 */
	public function setOptions(Array $options)
	{
		foreach ($options as $key => $value)
			$this->__set(str_replace(' ', '', ucwords(strtr($key, '_-', ' '))), $value);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBrdSeq()
	{
		return $this->_brd_seq;
	}

	/**
	 * @param mixed $brd_seq
	 */
	public function setBrdSeq($brd_seq)
	{
		$this->_brd_seq = $brd_seq;
	}

	/**
	 * @return mixed
	 */
	public function getBrdName()
	{
		return $this->_brd_name;
	}

	/**
	 * @param mixed $brd_name
	 */
	public function setBrdName($brd_name)
	{
		$this->_brd_name = $brd_name;
	}

	/**
	 * @return mixed
	 */
	public function getBrdDesc()
	{
		return $this->_brd_desc;
	}

	/**
	 * @param mixed $brd_desc
	 */
	public function setBrdDesc($brd_desc)
	{
		$this->_brd_desc = $brd_desc;
	}

	/**
	 * @return mixed
	 */
	public function getBrdSkin()
	{
		return $this->_brd_skin;
	}

	/**
	 * @param mixed $brd_skin
	 */
	public function setBrdSkin($brd_skin)
	{
		$this->_brd_skin = $brd_skin;
	}

	/**
	 * @return mixed
	 */
	public function getBrdFilePath()
	{
		return $this->_brd_file_path;
	}

	/**
	 * @param mixed $brd_file_path
	 */
	public function setBrdFilePath($brd_file_path)
	{
		$this->_brd_file_path = $brd_file_path;
	}

	/**
	 * @return mixed
	 */
	public function getPage()
	{
		return $this->_page;
	}

	/**
	 * @param mixed $page
	 */
	public function setPage($page)
	{
		$this->_page = $page;
	}

	/**
	 * @return mixed
	 */
	public function getRowCount()
	{
		return $this->_row_count;
	}

	/**
	 * @param mixed $row_count
	 */
	public function setRowCount($row_count)
	{
		$this->_row_count = $row_count;
	}

	/**
	 * View Render
	 */
	protected function getSkin()
	{
		$this->view->setScriptPath(
			APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR .
			'Zfeg' . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR . 'scripts'
		);
		$this->_helper->viewRenderer->renderBySpec(
			$this->getBrdSkin() . DIRECTORY_SEPARATOR . $this->getRequest()->getActionName(),
			array('controller' => 'board')
		);
	}

	/**
	 * Board List
	 * @todo not create
	 */
	protected function indexAction()
	{
		Zend_Filter_Input::ALLOW_EMPTY;
		$docs = new Zfeg_Model_Documents();
		$where['doc_brd_seq = ?'] = $this->getBrdSeq();
		$order = array('doc_idx', 'doc_depth');
		$rows = $docs->fetchPage($where, $order, null, $this->getPage(), $this->getRowCount());

		$this->view->entry = $rows['entry'];
		$this->view->paginator = $rows['paginator'];
		$this->getSkin();
	}

	/**
	 * Board View
	 * @todo not create
	 */
	protected function viewAction()
	{
		$this->getSkin();
	}

	/**
	 * Board Write
	 * @todo not create
	 */
	protected function writeAction()
	{
		$this->getSkin();
	}

	/**
	 * Board Save
	 * @todo not create
	 */
	protected function saveAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}

	/**
	 * Comment List
	 */
	protected function commentListAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$model = new Zfeg_Model_Comments();
		$doc_seq = $this->getRequest()->getParam('doc-seq');
		$where['cmt_doc_seq = ?'] = $doc_seq;
		$order = array('cmt_seq DESC');
		$offset = (intVal($this->getPage()) - 1) * intVal($this->getRowCount());
		$row = $model->fetchAll($where, $order, $this->getRowCount(), $offset);
		if ($row->toArray()) echo Zend_Json::encode($row->toArray());
	}

	/**
	 * Comment Save
	 * @todo not create
	 */
	protected function commentSaveAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}

	/**
	 * File Upload
	 * @todo not create
	 */
	protected function fileUploadAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}
} 