<?php

/**
 * Class Zfeg_Db_Table_Abstract
 */
class Zfeg_Db_Table_Abstract extends Zend_Db_Table_Abstract {

	/**
	 * @param null $where
	 * @param null $order
	 * @param null $group
	 * @param int $page
	 * @param int $rowCount
	 * @return array
	 * @throws Zend_Paginator_Exception
	 */
	public function fetchPage ($where = null, $order = null, $group = null, $page = 1, $rowCount = 10)
	{
		$query = $this->_where($this->select(), $where);
		if ($order !== null) $query->order($order);
		if ($group !== null) $query->group($group);
		$paginator = Zend_Paginator::factory($query);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage($rowCount);
		$entry = $paginator->getCurrentItems()->toArray();
		return array('entry' => $entry, 'paginator' => $paginator);
	}

	/**
	 * @param $pk
	 * @return mixed
	 * @throws Zend_Db_Table_Exception
	 * @throws Zend_Db_Table_Row_Exception
	 */
	public function fetchTree($pk)
	{
		$entry = $this->find($pk)->current();
		$result['entry'] = $entry->toArray();
		if ($this->_dependentTables) {
			foreach ($this->_dependentTables as $table => $object) {
				$result[$table] = $entry->findDependentRowset($object)->toArray();
			}
		}
		return $result;
	}

	/**
	 * @param $type
	 * @param $column
	 * @param null $where
	 * @param null $group
	 * @throws Exception
	 */
	public function fetchFunc($type, $column, $where = null, $group  = null)
	{
		$operator = array(
			'ABS',              // Return the absolute value
			'ACOS',             // Return the arc cosine
			'ASIN',             // Return the arc sine
			'ATAN',             // Return the arc tangent
			'ATAN2','ATAN',     // Return the arc tangent of the two arguments
			'AVG',              // Return the average value of the argument
			'BIT_AND',          // Return bitwise and
			'BIT_OR',           // Return bitwise or
			'BIT_XOR',          // Return bitwise xor
			'CEIL',             // Return the smallest integer value not less than the argument
			'CEILING',          // Return the smallest integer value not less than the argument
			'CONV',             // Convert numbers between different number bases
			'COS',              // Return the cosine
			'COT',              // Return the cotangent
			'COUNT',            // Return a count of the number of rows returned
			'CRC32',            // Compute a cyclic redundancy check value
			'DEGREES',          // Convert radians to degrees
			'EXP',              // Raise to the power of
			'FLOOR',            // Return the largest integer value not greater than the argument
			'GROUP_CONCAT',     // Return a concatenated string
			'LN',               // Return the natural logarithm of the argument
			'LOG',              // Return the natural logarithm of the first argument
			'LOG10',            // Return the base-10 logarithm of the argument
			'LOG2',             // Return the base-2 logarithm of the argument
			'MAX',              // Return the maximum value
			'MIN',              // Return the minimum value
			'MOD',              // Return the remainder
			'PI',               // Return the value of pi
			'POW',              // Return the argument raised to the specified power
			'POWER',            // Return the argument raised to the specified power
			'RADIANS',          // Return argument converted to radians
			'RAND',             // Return a random floating-point value
			'ROUND',            // Round the argument
			'SIGN',             // Return the sign of the argument
			'SIN',              // Return the sine of the argument
			'SQRT',             // Return the square root of the argument
			'STD',              // Return the population standard deviation
			'STDDEV',           // Return the population standard deviation
			'STDDEV_POP',       // Return the population standard deviation
			'STDDEV_SAMP',      // Return the sample standard deviation
			'SUM',              // Return the sum
			'TAN',              // Return the tangent of the argument
			'TRUNCATE',         // Truncate to specified number of decimal places
			'VAR_POP',          // Return the population standard variance
			'VAR_SAMP',         // Return the sample variance
			'VARIANCE',         // Return the population standard variance
		);
		if (in_array($type, $operator) === true) {
			$query = $this->_where($this->select(), $where);
			if ($group !== null) $query->group($group);
			$query->from($this->_name, array("result" => $type . '(' . $column . ')'));
			$result = $this->getAdapter()->fetchOne($query);
			if (is_numeric($result)) return $result + 0;
			else return $result;
		} else {
			throw new Exception('type Error : ' . $type . ' [' . implode("|", $operator) . ']');
		}
	}

	/**
	 * @param Array $data
	 * @return mixed
	 */
	public function insertUpdate(Array $data)
	{
		$query = $this->select();
		foreach($this->_primary as $key)
			$query->where($key . " =? ", $data[$key]);
		$row = $this->fetchRow($query);
		$this->getAdapter()->beginTransaction();
		try {
			if ($row) $row->setFromArray($data);
			else $row = $this->createRow($data);
			$result = $row->save();
			$this->getAdapter()->commit();
		} catch (Exception $e) {
			$this->getAdapter()->rollBack();
		}
		/** @noinspection PhpUndefinedVariableInspection */
		if (is_numeric($result)) return $result + 0;
		else return $result;
	}

}