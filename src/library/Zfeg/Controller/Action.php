<?php

class Zfeg_Controller_Action extends Zend_Controller_Action
{
	/**
	 * @var Zend_Auth
	 */
	protected $_auth;

	/**
	 * @var Int 1800 sec > 30 min
	 */
	protected $_auth_expire_time = 1800;

	/**
	 * @var Zend_Session_Namespace
	 */
	protected $_session;

	/**
	 * @var String
	 */
	protected $_session_namespace = "EG-SID";

	public function init()
	{
		parent::init();

		$this->setAuth(Zend_Auth::getInstance());

		if (!($this->getSession() instanceof Zend_Session_Namespace)) {
			$this->setSession(new Zend_Session_Namespace($this->getSessionNamespace()));
		}

		// extend auth session expire time
		if ($this->getAuth()->hasIdentity()) {
			$auth_session = new Zend_Session_Namespace($this->getAuth()->getStorage()->getNamespace());
			$auth_session->setExpirationSeconds($this->getAuthExpireTime());
		}
	}

	/**
	 * @return Zend
	 */
	public function getAuth()
	{
		return $this->_auth;
	}

	/**
	 * @param Zend_Auth $auth
	 */
	protected function setAuth(Zend_Auth $auth)
	{
		$this->_auth = $auth;
	}

	/**
	 * @return Int
	 */
	protected function getAuthExpireTime()
	{
		return $this->_auth_expire_time;
	}

	/**
	 * @param Int $auth_expire_time
	 */
	protected function setAuthExpireTime($auth_expire_time)
	{
		$this->_auth_expire_time = $auth_expire_time;
	}

	/**
	 * @return Zend_Session_Namespace
	 */
	protected function getSession()
	{
		return $this->_session;
	}

	/**
	 * @param Zend_Session_Namespace $session
	 */
	protected function setSession(Zend_Session_Namespace $session)
	{
		$this->_session = $session;
	}

	/**
	 * @return String
	 */
	protected function getSessionNamespace()
	{
		return $this->_session_namespace;
	}

	/**
	 * @param String $session_namespace
	 */
	protected function setSessionNamespace($session_namespace)
	{
		$this->_session_namespace = $session_namespace;
	}


} 