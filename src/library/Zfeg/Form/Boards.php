<?php

class Zfeg_Form_Boards extends Zfeg_Form
{
	public function init()
	{
		$this->addElement(
			'text',
			'brd_seq',
			array(
				'required' => false,
				'filters' => array('StringTrim', 'Int'),
				'validators' => array(
					array(
						'validator'=>'Digits',
						'breakChainOnFailure'=>true,
						'options' => array(
							'min' => 0,
							'messages' => 'Invalid value'
						)
					)
				)
			)
		);

		$this->addElement(
			'text',
			'brd_name',
			array(
				'required' => true,
				'required' => true,
				'filters' => array('StringTrim'),
				'validators' => array(
					array(
						'validator'=>'NotEmpty',
						'breakChainOnFailure'=>true,
					)
				)
			)
		);

		$this->addElement(
			'text',
			'brd_skin',
			array(
				'required' => true,
				'required' => true,
				'filters' => array('StringTrim'),
				'validators' => array(
					array(
						'validator'=>'NotEmpty',
						'breakChainOnFailure'=>true,
					)
				)
			)
		);
	}
}
