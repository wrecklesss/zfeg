<?php

/**
 * Class Zfeg_Model_Comments
 */
class Zfeg_Model_Comments extends Zfeg_Db_Table_Abstract
{
	protected $_name = 'comments';
	protected $_primary = array('cmt_seq');
	protected $_referenceMap = array(
		'fk_comments_documents' => array(
			'columns'       => array('cmt_doc_seq'),
			'refTableClass' => 'Application_Model_Documents',
			'refColumns'    => array('usr_seq'),
			'onUpdate'      => self::CASCADE,
			'onDelete'      => self::SET_NULL
		)
	);
	protected $_dependentTables = null;
}