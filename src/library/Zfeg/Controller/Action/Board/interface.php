<?php

interface Zfeg_Controller_Action_Board_interface {

	/**
	 * @param $name
	 * @param $value
	 * @return mixed
	 */
	public function __set($name, $value);

	/**
	 * @param $name
	 * @return mixed
	 */
	public function __get($name);

	/**
	 * @param array $options
	 * @return mixed
	 */
	public function getOptions(Array $options);

	/**
	 * @param array $options
	 * @return mixed
	 */
	public function setOptions(Array $options);
} 