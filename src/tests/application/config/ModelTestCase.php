<?php

/**
 * Class Test_Application_Config_ModelTestCase
 * @property Zend_Application bootstrap
 * @property Zend_Config_Ini __configuration
 */
class Test_Application_Config_ModelTestCase extends Zend_Test_PHPUnit_DatabaseTestCase
{
	protected $_connectionMock;
	private $__configuration = null;
	protected $_connectionSchema;
	protected $_seedFilesPath;
	protected $dataSet;

	public function __construct()
	{
		$this->dataSet = new Zend_Test_PHPUnit_Db_DataSet_QueryDataSet($this->getConnection());
	}

	public function getConfiguration()
	{

		if ($this->__configuration == null) {
			$this->__configuration = new Zend_Config_Ini(APPLICATION_PATH . '/configs/' . APPLICATION_NAMESPACE . '.ini');
		}

		var_dump($this->__configuration);

		return $this->__configuration;
	}

	public function getSeedFilesPath()
	{
		if ($this->_seedFilesPath == null) {
			$this->_seedFilesPath = $this->getConfiguration()->tests->seeds->folder;
		}

		return rtrim($this->_seedFilesPath, '/') . '/';
	}

	protected function getConnection()
	{
		if ($this->_connectionMock == null) {
			$dbAdapterName = $this->getConfiguration()->tests->dbadapter;
			$dbAdapterParams = $this->getConfiguration()->tests->dbparams->toArray();

			$connection = Zend_Db::factory($dbAdapterName, $dbAdapterParams);

			$this->_connectionMock = $this->createZendDbConnection(
				$connection, $this->_connectionSchema
			);

			Zend_Db_Table_Abstract::setDefaultAdapter($connection);
		}
		return $this->_connectionMock;
	}

	protected function getDataSet()
	{
		return $this->createFlatXMLDataSet($this->_seedFilesPath . 'seed.xml');
	}
} 