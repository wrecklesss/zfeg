<?php
/**
 * Class Test_Application_Module_Default_
 */
class Test_Application_Module_Default_IndexControllerTest extends Test_Application_Config_ControllerTestCase
{

	public function testIndexAction()
	{
		$this->dispatch("/");
		// assertions
		$this->assertModule('default');
		$this->assertController('index');
		$this->assertAction('index');
		$this->assertResponseCode(200);
	}

} 