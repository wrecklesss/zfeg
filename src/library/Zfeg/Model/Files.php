<?php

/**
 * Class Zfeg_Model_Files
 */
class Zfeg_Model_Files extends Zfeg_Db_Table_Abstract
{
	protected $_name = 'files';
	protected $_primary = array('fil_idx');
	protected $_referenceMap = array(
		'fk_files_documents' => array(
			'columns'       => array('fil_doc_seq'),
			'refTableClass' => 'Application_Model_Documents',
			'refColumns'    => array('usr_seq'),
			'onUpdate'      => self::CASCADE,
			'onDelete'      => self::CASCADE
		)
	);
	protected $_dependentTables = null;
}