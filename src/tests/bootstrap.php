<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing'));

// Define application environment service namespace setting
defined('APPLICATION_NAMESPACE')
|| define('APPLICATION_NAMESPACE', (getenv('APPLICATION_NAMESPACE') ? getenv('APPLICATION_NAMESPACE') : 'default' ));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

require_once 'application/config/ControllerTestCase.php';
require_once 'application/config/ModelTestCase.php';