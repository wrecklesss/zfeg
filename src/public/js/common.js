/**
 * @author TaeGuNi hjt1761@gmail.com
 */

$(document).ready(function() {
    /**
     * breadcrumb active class auto loader
     */
    $(".breadcrumb").children().last("li").addClass("active");
});