<?php

try {
	// Define path to application directory
	defined('APPLICATION_PATH')
		|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application' ));

	// Define application environment [development, testing, staging, production]
	defined('APPLICATION_ENV')
		|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development' ));

	// Define application environment service namespace setting
	defined('APPLICATION_NAMESPACE')
		|| define('APPLICATION_NAMESPACE', (getenv('APPLICATION_NAMESPACE') ? getenv('APPLICATION_NAMESPACE') : 'default' ));

	// Ensure library/ is on include_path
	set_include_path(implode(PATH_SEPARATOR, array(
		realpath(APPLICATION_PATH . '/../library'),
		get_include_path(),
	)));

	/** Zend_Application */
	require_once 'Zend/Application.php';

	// Create application, bootstrap, and run
	$application = new Zend_Application(
		APPLICATION_ENV,
		APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . APPLICATION_NAMESPACE . '.ini'
	);
	$application->bootstrap()
				->run();

} catch (Exception $e) {
	echo $e->getMessage();
}