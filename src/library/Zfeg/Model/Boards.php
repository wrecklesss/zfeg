<?php

/**
 * Class Zfeg_Model_Boards
 */
class Zfeg_Model_Boards extends Zfeg_Db_Table_Abstract
{
	protected $_name = 'boards';
	protected $_primary = array('brd_seq');
	protected $_referenceMap = null;
	protected $_dependentTables = array(
		'comments' => 'Application_Model_Documents'
	);
}