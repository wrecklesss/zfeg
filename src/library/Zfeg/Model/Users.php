<?php

/**
 * Class Zfeg_Model_Users
 */
class Zfeg_Model_Users extends Zfeg_Db_Table_Abstract
{
	protected $_name = 'users';
	protected $_primary = array('usr_seq');
	protected $_referenceMap = null;
	protected $_dependentTables = array(
		'comments' => 'Application_Model_Documents'
	);
}