<?php
/**
 * Class Test_Application_Config_ControllerTestCase
 */
class Test_Application_Config_ControllerTestCase extends Zend_Test_PHPUnit_ControllerTestCase
{

	public function setUp()
	{
		$this->bootstrap = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/' . APPLICATION_NAMESPACE . '.ini');
		parent::setUp();
	}

} 