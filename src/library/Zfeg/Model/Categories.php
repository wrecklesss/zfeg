<?php

/**
 * Class Zfeg_Model_Categories
 */
class Zfeg_Model_Categories extends Zfeg_Db_Table_Abstract
{
	protected $_name = 'categories';
	protected $_primary = array('cat_seq');
	protected $_referenceMap = null;
	protected $_dependentTables = array(
		'comments' => 'Application_Model_Documents'
	);
}