<?php

/**
 * Class Zfeg_Application_Bootstrap_Bootstrap
 */
class Zfeg_Application_Bootstrap_Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

	final protected function _initEg()
	{
		$this->bootstrap('db');
		$this->bootstrap('session');
		Zend_Session::start();
	}

	protected function _initNavigation()
	{
		if (is_file(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . APPLICATION_NAMESPACE . '.sitemap.xml')) {
			$container = new Zend_Navigation(
				new Zend_Config_Xml(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . APPLICATION_NAMESPACE . '.sitemap.xml', 'nav')
			);
			$this->bootstrap('layout');
			$layout = $this->getResource('layout');
			$view = $layout->getView();
			$view->navigation($container);
		}
	}

}