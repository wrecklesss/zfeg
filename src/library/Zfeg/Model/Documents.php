<?php

/**
 * Class Zfeg_Model_Documents
 */
class Zfeg_Model_Documents extends Zfeg_Db_Table_Abstract
{
	protected $_name = 'documents';
	protected $_primary = array('doc_seq');
	protected $_referenceMap = array(
		'fk_documents_users' => array(
			'columns'       => array('doc_usr_seq'),
			'refTableClass' => 'Application_Model_Users',
			'refColumns'    => array('usr_seq'),
			'onUpdate'      => self::CASCADE,
			'onDelete'      => self::SET_NULL
		),
		'fk_documents_boards' => array(
			'columns'       => array('doc_brd_seq'),
			'refTableClass' => 'Application_Model_Boards',
			'refColumns'    => array('cat_seq'),
			'onUpdate'      => self::CASCADE,
			'onDelete'      => self::RESTRICT
		),
		'fk_documents_categories' => array(
			'columns'       => array('doc_cat_seq'),
			'refTableClass' => 'Application_Model_Categories',
			'refColumns'    => array('cat_seq'),
			'onUpdate'      => self::CASCADE,
			'onDelete'      => self::SET_NULL
		)
	);
	protected $_dependentTables = array(
		'comments' => 'Application_Model_Comments',
		'files'    => 'Application_Model_Files'
	);
} 